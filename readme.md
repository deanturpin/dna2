# BRIEF: open a text file of DNA bases and return a string of results

See [DNA sequencing](https://en.wikipedia.org/wiki/DNA_sequencing). The large
file used in these tests is downloaded from
[here](ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/dna/). This is
an exercise in getting the most out of a single thread.

# FASTA file format
> In bioinformatics and biochemistry, the FASTA format is a text-based format
> for representing either nucleotide sequences or amino acid (protein)
> sequences, in which nucleotides or amino acids are represented using
> single-letter codes. The format also allows for sequence names and comments
> to precede the sequences. The format originates from the FASTA software
> package, but has now become a near universal standard in the field of
> bioinformatics.

See the [FASTA](https://en.wikipedia.org/wiki/FASTA_format) file format.

# Performance strategies
- Initialising the map with expected bases
- Using a `std::vector` rather than a `std::map`
- Counting lines of all the same character (`std::all_of`)
- Counting sequencess of the same character before pushing to the map
- Skipping the first line to remove the check for a comment in the main loop
- Don't do it in parallel :)
- Don't use `std::stringstream`
- Read file into a string in one go: `const std::string
  contents{std::istreambuf_iterator<char>(in), {}};`
- Use `std::unordered_map`

# To try
- `std::ios::sync_with_stdio(false)`
- Don't use `std::string` for the buffer - array on the stack?

# References
- [How to read a file in
  C](https://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html)
- https://stackoverflow.com/questions/17925051/fast-textfile-reading-in-c
- [Memory mapped file example](http://coliru.stacked-crooked.com/view?id=587822cfa428a85c60e1dc3866ac8a3c-e223fd4a885a77b520bbfe69dda8fb91)
- Kernel access patterns: `posix_fadvise(fd, 0, 0, 1);  // FDADVICE_SEQUENTIAL`

