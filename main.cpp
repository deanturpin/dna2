#include <algorithm>
#include <chrono>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <unordered_map>

// Each routine reads this file to avoid passing it in as a param
const std::string file{"dna.fa"};

// See results string for a summary of each version of the routine

std::string sequence0() {

  // Open file
  std::ifstream in{file, std::ios::binary};

  // Skip first line
  std::string comment;
  std::getline(in, comment);

  std::map<std::string, size_t> histogram;
  std::string line;
  size_t line_count{9};
  while (std::getline(in, line)) {
    ++histogram[line];
    ++line_count;
  }

  std::stringstream results;
  results << "sequence using std::map, " << histogram.size() << " entries, "
          << line_count << " lines\n";

  return results.str();
}

std::string sequence1() {

  // Open file
  std::ifstream in{file, std::ios::binary};

  // Skip first line
  std::string comment;
  std::getline(in, comment);

  std::unordered_map<std::string, size_t> histogram;
  std::string line;
  size_t line_count{9};
  while (std::getline(in, line)) {
    ++histogram[line];
    ++line_count;
  }

  std::stringstream results;
  results << "sequence using std::unordered_map, " << histogram.size()
          << " entries, " << line_count << " lines\n";

  return results.str();
}

std::string histogram0() {

  // Open file
  std::ifstream in{file, std::ios::binary};

  // Skip first line
  std::string comment;
  std::getline(in, comment);

  // Create empty histogram
  std::map<char, size_t> histogram;

  // Histogram all bases on the subsequent lines
  std::string line;
  while (std::getline(in, line))
    for (const auto &base : line)
      ++histogram[base];

  // Generate results
  std::stringstream results;
  results << "std::map\n";
  for (const auto &[base, count] : histogram)
    results << base << "\t" << count << "\n";

  return results.str();
}

std::string histogram1() {

  // Open file
  std::ifstream in{file, std::ios::binary};

  // Skip first line
  std::string comment;
  std::getline(in, comment);

  // Create empty histogram
  std::unordered_map<char, size_t> histogram;

  // Histogram all bases on the subsequent lines
  std::string line;
  while (std::getline(in, line))
    for (const auto &base : line)
      ++histogram[base];

  // Generate results
  std::stringstream results;
  results << "std::unordered_map\n";
  for (const auto &[base, count] : histogram)
    results << base << "\t" << count << "\n";

  return results.str();
}

std::string histogram2() {

  // Create empty histogram
  std::array<size_t, 10> histogram;
  std::ranges::fill(histogram, 0);

  // Open the file
  std::ifstream in{file, std::ios::binary};

  // Skip the first comment line
  std::string line;
  std::getline(in, line);

  // Process each line and count occurances of each base
  while (std::getline(in, line))
    for (const char &base : line) {
      switch (base) {
      case 'A':
        ++histogram[0];
        break;
      case 'C':
        ++histogram[1];
        break;
      case 'G':
        ++histogram[2];
        break;
      case 'N':
        ++histogram[3];
        break;
      case 'T':
        ++histogram[4];
        break;
      case 'a':
        ++histogram[5];
        break;
      case 'c':
        ++histogram[6];
        break;
      case 'g':
        ++histogram[7];
        break;
      case 'n':
        ++histogram[8];
        break;
      case 't':
        ++histogram[9];
        break;

      default:
        break;
      }
    }

  // Generate results
  std::stringstream results;
  results << "std::array\n";
  for (const auto &h : histogram)
    if (h > 0)
      results << h << "\n";

  return results.str();
}

std::string read0() {

  std::ifstream in{file};
  std::string line;
  while (std::getline(in, line)) { /* process line */
  }

  return "read using ifstream and getline\n";
}

std::string read0bin() {

  std::ifstream in{file, std::ios::binary};
  std::string line;
  while (std::getline(in, line)) { /* process line */
  }

  return "read using ifstream and getline (ios::binary)\n";
}

std::string read1() {

  std::stringstream ss;
  ss << std::ifstream{file}.rdbuf();

  std::string line;
  while (std::getline(ss, line)) { /* process line */
  }

  return "read using stringstream and rdbuf\n";
}

std::string read2() {

  std::ifstream in{file};
  const std::string str{std::istream_iterator<char>{in}, {}};

  for ([[maybe_unused]] const auto &c : str) { /* process line */
  }

  return "create string using istream_iterator\n";
}

std::string read2bin() {

  std::ifstream in{file, std::ios::binary};
  const std::string str{std::istream_iterator<char>{in}, {}};

  for ([[maybe_unused]] const auto &c : str) { /* process line */
  }

  return "create string using istream_iterator (std::ios::binary)\n";
}

std::string read3() {

  std::ifstream is(file);

  // Get length of file
  is.seekg(0, std::ios::end);
  const int length = is.tellg();
  is.seekg(0, std::ios::beg);

  // Allocate memory
  char *buffer = new char[length];

  // Read data as a block
  is.read(buffer, length);
  for (int c = 0; c < length; ++c) { /* process line */
  }

  // Tidy up
  is.close();
  delete[] buffer;

  return "seek and copy into stack buffer\n";
}

std::string read3bin() {

  std::ifstream is(file, std::ios::binary);

  // Get length of file
  is.seekg(0, std::ios::end);
  const int length = is.tellg();
  is.seekg(0, std::ios::beg);

  // Allocate memory
  char *buffer = new char[length];

  // Read data as a block
  is.read(buffer, length);
  for (int c = 0; c < length; ++c) { /* process line */
  }

  // Tidy up
  is.close();
  delete[] buffer;

  return "seek and copy into stack buffer (std::ios::binary)\n";
}

int main() {
  using hrc = std::chrono::high_resolution_clock;
  // Get the size of the file
  const std::uintmax_t file_size = std::filesystem::file_size(file);

  const auto time_func = [&](const auto &func) {
    const auto start = hrc::now();
    const std::string results = func();
    const auto stop = hrc::now();

    const double duration = std::chrono::duration<double>(stop - start).count();
    const double bandwidth = 8 * (file_size / duration) / std::pow(2, 20);

    std::stringstream ss;
    ss << results << duration << " seconds, " << bandwidth << " Mib/s\n\n";

    return ss.str();
  };

  // Time each version of the sequence function on the same data
  for (const auto &func : {sequence0, sequence1})
    std::cout << time_func(func);

  // Time each version of the read function on the same data
  for (const auto &func :
       {read0bin, read3bin, read3, read0, read1, read2, read2bin})
    std::cout << time_func(func);

  // Time each version of the histogram function on the same data
  for (const auto &func : {histogram0, histogram1, histogram2})
    std::cout << time_func(func);
}
