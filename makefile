# Get test file, build and run
all: dna.fa main.o run

DNA_FILE = ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.chromosome.2.fa.gz 

# Get the test file
dna.fa:
	curl $(DNA_FILE) | gunzip > dna.fa
	@ls -lh $@

# Compiler of choice
CXX := g++-10

# Dependencies
apt:
	apt update
	apt install --yes g++-10 curl \
		time cppcheck valgrind binutils # for gprof

# Some useful compiler flags not set by default
CXXFLAGS ?= --std=c++20 --all-warnings --extra-warnings --pedantic-errors \
	-Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wdeprecated-copy -Wconversion \
	-Wall -Wextra -Wpedantic \
	-Og \
	-Wattribute-alias -Wformat-overflow -Wformat-truncation -Wclass-conversion \
	-Wmissing-attributes -Wstringop-truncation $(EXTRA_FLAGS)

# Make an object from a cpp
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ $<

# Run and time
run: main.o
	/usr/bin/time ./$<

# Tidy up
clean:
	rm -f *.o dna.fa

##########
# TOOLS
##########

format:
	clang-format-10 -i *.cpp

valgrind: main.o
	@echo CACHE
	valgrind --tool=cachegrind ./$<
	@echo MEMORY LEAK
	valgrind --leak-check=full ./$<

gprof:
	@rm -f main.o
	$(MAKE) EXTRA_FLAGS="-pg"
	@./main.o
	@gprof main.o gmon.out

lint:
	cppcheck --enable=all main.cpp

